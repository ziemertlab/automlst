Automatic Multi-Locus Species Tree (autoMLST) Overview
====================================================
autoMLST is a webserver and analysis pipeline for constructing species phylogenies from genomic data. The pipeline scans for conserved single copy housekeeping genes, selects comparable genomes in the database, and builds a phylogeny using either a concatenated gene matrix or coalescent tree method.

##################### AutoMLST analysis server local command-line install #####################

********** Required Applications **********
Denovo workflow:
Mafft, trimal, hmmsearch, iqtree, prodigal, MASH

Accelerated workflow:
ASTRAL, RaxML

********** Prerequisites **********

- - git

	To check if git is installed:
		git —-version

	If an installation is necessary, please follow the instructions under: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

	Please make sure git is in your $PATH, with:
		echo $Path

	If it is not, add it with (e.g. it is installed under /usr/bin/git):
		PATH=$PATH:/usr/bin/git


- - ana/miniconda

	To check if a version of conda is installed:
		conda —-version

	If an installation is necessary, please follow the instructions under: https://conda.io/projects/conda/en/latest/user-guide/install/index.html?highlight=conda

	Please make sure git is in your $PATH, with:
		echo $Path

	If it is not, add it with (e.g. it is installed under /Users/yourname/anaconda2/bin/conda):
		PATH=$PATH:/Users/yourname/anaconda2/bin/conda


********** Installation **********

1. go to the directory where autoMLST is to be installed, and execute:
	git clone https://bitbucket.org/ziemertlab/automlst.git

2. to avoid future issues, update all conda packages with:
	conda update —-all

3. go into this new directory, with:
	cd automlst

4. create the automlst conda environment, with:
	conda env create --file environment.yml

5. Download required sql-database (~25GB free space required):
    wget http://automlst.ziemertlab.com/static/refseqreduced.db

    *if download stops/fails continue with:
    wget --continue http://automlst.ziemertlab.com/static/refseqreduced.db

6. Decompress reference sets (required for accelerated workflow):
    tar -xzf automlstrefs.tar.gz

********** Usage **********

To use the tool, one should first activate the conda environment created for automlst, with:
	conda activate automlst

Then simply navigate to the automlst folder and execute the automlst.py script (e.g. to read the script’s help information), with:
	python automlst -h

	*note you will need to specify the reference sql-database (--refdb) and reference folder (--refdirectory) from step 5 & 6

You can deactivate the automlst conda environment, with:
	conda deactivate