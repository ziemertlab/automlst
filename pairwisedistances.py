#!/usr/bin/env python
# 2018 Mohammad Alanjary
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of autoMLST
# autoMLST is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with ARTS
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import glob, argparse, sqlite3, itertools
from sqlite3 import DatabaseError
from scipy import stats
from ete3 import Tree

def getGCFid(name):
    gcfindex = name.find('GCF_')
    return name[gcfindex:gcfindex+13]

def getLeafDists(treefile):
    T = Tree(treefile)
    OG = None
    #Ensure rooted
    for leaf in T:
        if "OG--" in leaf.name:
            OG = leaf.name
    if OG:
        T.set_outgroup(OG)

    results = {}
    leaves = [x.name for x in T.get_leaves()]
    combos = itertools.combinations(leaves,2)
    for pair in combos:
        #Store pair as sorted concatenated string
        results['@'.join(sorted([getGCFid(x) for x in pair]))] = T.get_distance(pair[0],target2=pair[1])

    return results,leaves

def checkAll(treefiles,anitable,outfile,distfile):
    """Run getLeafDists for every tree and lookup corresponding ANI distances"""
    treefiles = glob.glob(treefiles)

    #Convert tsv file to sqlite3 if not already
    try:
        conn = sqlite3.connect(anitable)
        results = conn.execute("Select * From ANIscores")
    except (ValueError,DatabaseError):
        #Not a sqlite db try and create one
        conn = sqlite3.connect(outfile+".sqdb")
        conn.execute("CREATE TABLE ANIscores (pairs text, score real)")
        print "Converting to sqliteDB: %s"%(outfile+".sqdb")
        with open(anitable,"r") as fil:
            for line in fil:
                x = line.strip().split()
                score = float(x[-1])
                pair = "@".join(sorted(x[:2]))
                conn.execute("INSERT INTO ANIscores VALUES (?,?)",[pair,score])
        conn.execute("CREATE INDEX pair ON ANIscores(pairs)")
        conn.commit()

    with open(outfile,"w") as ofil:
        #Write header
        ofil.write("#Treefile\tCoverage_all\tPearson_all\tPval_all\tCoverage_90\tPearson_90\tPval_90\n")
        #loop through all trees
        for treefil in treefiles:
            print "started tree: %s"%treefil
            results,leaves = getLeafDists(treefil)
            leafset = set("@".join([x for x in results.keys()]).split("@"))
            print "reading from db..."
            dbresults = conn.execute("SELECT * FROM ANIscores WHERE pairs IN (%s)"%','.join("?"*len(results.keys())),results.keys()).fetchall()
            dbset = set("@".join([x[0] for x in dbresults]).split("@"))


            allresults = {x[0]:[results.get(x[0],"N/A"),x[1]] for x in dbresults}
            #calculate stats for all and at >= 0.9
            cov1 = float(len(dbset))/len(leafset)
            A = [x[0] for x in allresults.values()]
            B = [x[1] for x in allresults.values()]
            pearson1,pval1 = stats.pearsonr(A,B)

            dbset = set("@".join([k for k,x in allresults.items() if float(x[1]) >= 0.9]).split("@"))
            cov2 = float(len(dbset))/len(leafset)
            A = [x[0] for x in allresults.values() if float(x[1]) >= 0.9]
            B = [x[1] for x in allresults.values() if float(x[1]) >= 0.9]
            pearson2,pval2 = stats.pearsonr(A,B)

            ofil.write("%s\t%s\t%s\t%s\t%s\t%s\t%s\n"%(treefil,cov1,pearson1,pval1,cov2,pearson2,pval2))

            if distfile:
                with open(treefil+"."+distfile,"w") as dfil:
                    allresults = ["%s\t%s\t%s"%(x[0],results.get(x[0],"N/A"),x[1]) for x in dbresults]
                    dfil.write("#From tree: %s #Leaves: %s #ANI_Leaves: %s #Coverage: %s\n"%(treefil,len(leaves),len(dbset),cov1))
                    dfil.write("\n".join(allresults)+"\n")
                        # for p in results.keys():
                        #     dbresult = conn.execute("SELECT * FROM ANIscores WHERE pair='%s' LIMIT 1"%p).fetchall()
                        #     sys.stdout.write('.')
                        #     if len(dbresult):
                        #         dfil.write("%s\t%s\t%s\n"%(p,results.get(p,"N/A"),dbresult[0][1]))

            # newresults = {x[0]:[treefil,results.get([x[0]],"N/A"),x[1]] for x in dbresults}
    conn.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Check tree for proper clading of ANI groups")
    parser.add_argument("input",nargs='?',help="Tree files expression ex: directory/*.tree default=*.tree",default="*.tree")
    parser.add_argument("-o","--out",help="Store results in OUT (default: alldistances.tsv)",default="alldistances.tsv")
    parser.add_argument("-d","--dists",help="Store results in distance files (default: none) ex: 'dist.tsv' will generate *.dist.tsv",default="")
    parser.add_argument("-at","--anitable",help="ANI table. Sqlite file of distances or tsv file",default="")
    args = parser.parse_args()
    checkAll(args.input,args.anitable,args.out,args.dists)