#!/usr/bin/env python
# 2018 Mohammad Alanjary
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
# Funding by the German Centre for Infection Research (DZIF)
#
# This file is part of autoMLST
# autoMLST is free software. you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version
#
# License: You should have received a copy of the GNU General Public License v3 with ARTS
# A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.


from ete3 import Tree

def rerootTree(treefile,output,ogterm="OG--",fmat=3):
    intree = Tree(treefile)
    og = [x for x in intree.get_leaves() if x.name.startswith(ogterm)]
    if not len(og):
        return False

    og = og[0]
    intree.set_outgroup(og.name)
    intree.ladderize(direction=1)
    intree.write(outfile=output,format=fmat)
    return True